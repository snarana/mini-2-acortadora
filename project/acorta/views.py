from django.shortcuts import render, redirect
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from .models import content_list
from urllib.parse import unquote

# Create your views here.


def get_resource(request, id_recurso):
    try:
        url_short = content_list.objects.get(short=id_recurso)
        return redirect(url_short.url)
    except content_list.DoesNotExist:
        raise Http404(f'<h1>El recurso no está disponible<h1>')


def found():
    p = 1
    while content_list.objects.filter(short=str(p)).exists():
        p+=1
    return p


@csrf_exempt
def index(request):
    if request.method == 'GET':
        urls = content_list.objects.all()
        urls_s = {'urls': urls}
        return render(request, 'acorta/index.html', urls_s)
    elif request.method == 'POST':
        urls = content_list.objects.all()
        url_qs = request.POST['url']
        prueba = unquote(url_qs)
        if prueba.startswith(('http://', 'https://')):
            url = content_list(short=request.POST['short'], url=prueba)
            url.save()
        else:
            pre = 'https://'
            prueba = pre + prueba
        if not request.POST['short']:
            s = found()
            url = content_list(short=str(s), url=prueba)
            url.save()
        return render(request, 'acorta/index.html', {'urls': urls})
