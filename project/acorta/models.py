from django.db import models

# Create your models here.
class content_list(models.Model):
    url: str = models.CharField(max_length=32)
    short = models.CharField(max_length=32)

    def __str__(self):
        return self.short
